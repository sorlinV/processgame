pub fn crypt(input: &String) -> String {
    let mut sha:Sha256 = Sha256::new();
    sha.input_str(input);
    sha.result_str()
}

#[derive(Serialize, Deserialize)]
pub struct User {
    user_id: i64,
    username: String,
    password: String,
    salt: String,
    mail: String,
    name: String,
    lastname: String,
    desciption: String,
    token: String,
}

impl User {
    pub fn new(_username:String, _password:String, _mail:String, _name:String, _lastname:String, _description:String) -> User {
        let rand_str = rand::random::<u64>().to_string();
        let mut salt:String = crypt(&rand_str);
        User {
            user_id: 0,
            username: _username,
            password: crypt(&format!("{}{}", _password, salt)),
            salt: salt,
            mail: _mail,
            name: _name,
            lastname: _lastname,
            desciption: _description,
            token: "".to_owned()
        }
    }

    pub fn insert(&self) -> User {
        sql!(format!("insert into user(username, password, salt, mail, name, lastname, desciption, token) values ({}, {}, {}, {}, {}, {}, {}, {})",
                    self.username, self.password, self.salt, self.mail, self.name, self.lastname, self.desciption, self.token))
    }

    pub fn select(&self, _id_user:String) -> User {
        sql!(format!("select * from user where id_user = {}", _id_user))
    }

    pub fn delete(&self, _id_user:String) {
        sql!(format!("delete user where id_user = {}", _id_user));
    }

    pub fn update(&self, _id_user:String) {
        sql!(format!("update user set username = {}, password = {}, salt = {}, mail = {}, name = {}, lastname = {}, desciption = {}, token = {} where id_user = {}",
                    self.username, self.password, self.salt, self.mail, self.name, self.lastname, self.desciption, self.token, _id_user));
    }
}