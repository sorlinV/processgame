#![feature(plugin, decl_macro)]
#![plugin(rocket_codegen)]
extern crate rocket;
extern crate serde;
extern crate serde_json;
extern crate mysql;
extern crate crypto;

#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate lazy_static;

use mysql::*;
use serde_json::Error;
use message::*;
use classes::*;
use user::*;
use crypto::sha2::Sha256;

#[macro_use]
mod my_macro;
mod message;
mod classes;
mod user;

lazy_static! {
    pub static ref DB:Pool = Pool::new("").unwrap();
}


#[get("/")]
fn user_get_all(id:u32) -> String {
    println!("{}", id);
    message("User create", MESSAGE_OK)
}

#[get("/<id>")]
fn user_get(id:u32) -> String {
    println!("{}", id);
    message("User create", MESSAGE_OK)
}

#[post("/<id>")]
fn user_post(id:u32) -> String {
    println!("{}", id);
    message("User create", MESSAGE_OK)
}

#[patch("/")]
fn user_update() -> String {
    message("User create", MESSAGE_OK)
}

#[delete("/<id>")]
fn user_delete(id:u32) -> String {
    println!("{}", id);
    message("User create", MESSAGE_OK)
}

fn main() {
    rocket::ignite()
        .mount("/user", routes![user_get])
        .mount("/user", routes![user_post])
        .mount("/user", routes![user_update])
        .mount("/user", routes![user_delete])
        .launch();
}