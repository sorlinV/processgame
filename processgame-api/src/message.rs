pub static MESSAGE_OK:u8 = 0;
pub static MESSAGE_ERROR:u8 = 1;
pub static MESSAGE_WARNING:u8 = 2;
pub static MESSAGE_HIDDEN:u8 = 3;

#[derive(Serialize, Deserialize)]
pub struct Message {
    type_: u8,
    message: String,
}

pub fn message(_message: &str, _type_:u8) -> String
{
    let mes = Message {
        type_: _type_,
        message: _message.to_owned()
    };
    let mes_str = match serde_json::to_string(&mes) {
        Ok(mes) => {mes}
        Err(err) => {format!("{}{}{}", r#"{"type_": 1, "message": "error de message ("#, err,r#")"}"#)}
    };
    format!("{}", mes_str)
}