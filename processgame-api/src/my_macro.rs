#[macro_export]
macro_rules! sql {
    ( $x:expr ) => {
        DB.prep_exec(x)
            .map(|result| {
                result.map(|x| x.unwrap()).map(|row| {
                    my::from_row(row);
                }).collect()
            }).unwrap();
    };
}