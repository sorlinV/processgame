extern crate crypto;
extern crate rand;

use classes::crypto::digest::Digest;
use classes::crypto::sha2::Sha256;
use classes::rand::*;
pub fn crypt(input: &String) -> String {
    let mut sha:Sha256 = Sha256::new();
    sha.input_str(input);
    sha.result_str()
}
#[derive(Serialize, Deserialize)]
struct Cart {
    user_id: i64,
    game_id: i64,
    quantity: i64,
}

impl Cart {
    pub fn new(_user_id:i64, _game_id:i64, _quantity:i64) -> Cart
    {
        Cart {
            user_id: _user_id,
            game_id: _game_id,
            quantity: _quantity
        }
    }

    pub fn insert(_this: &Cart) -> Cart {
        sql!(format!("insert into cart value ({}, {}, {})",
        _this.user_id, _this.game_id, _this.quantity))
    }

    pub fn select_all(_this: &Cart) -> Cart {
        sql!(format!("select * from cart where user_id = {}",
        _this.user_id))
    }

    pub fn delete(_this: &Cart) -> Cart {
        sql!(format!("delete from cart where user_id = {} and game_id = {}",
        _this.user_id, _this.game_id))
    }

    pub fn update_qty(_this: &Cart) -> Cart {
        sql!(format!("update cart set quantity = {} where user_id = {} and game_id = {}",
        _this.quantity, _this.user_id, _this.game_id))
    }
}

#[derive(Serialize, Deserialize)]
struct Counterpart {
    counterpart_id: i64,
    crowdfunding_id: i64,
    price: f64,
    title: String,
    desciption: String,
}

impl Counterpart {
    pub fn new(counterpart_id:i64, crowdfunding_id:i64, price:f64, title:String, desciption:String) -> Counterpart
    {
        Counterpart {
            counterpart_id,
            crowdfunding_id,
            price,
            title,
            desciption
        }
    }

    pub fn insert(_this: &Counterpart) -> Vec<Counterpart> {
        sql!(format!("insert into counterpart value ({}, {}, {}, {}, {})",
        _this.counterpart_id, _this.crowdfunding_id, _this.price, _this.title, _this.desciption))
    }

    pub fn select_all(_this: &Counterpart) -> Vec<Counterpart> {
        sql!(format!("select * from counterpart where crowdfunding_id = {}",
        _this.crowdfunding_id))
    }

    pub fn select_all_id(_crowdfunding_id:i64) -> Vec<Counterpart> {
        sql!(format!("select * from counterpart where crowdfunding_id = {}",
        _crowdfunding_id))
    }

    pub fn select_one(_this: &Counterpart) -> Vec<Counterpart> {
        sql!(format!("select * from counterpart where counterpart_id = {}",
        _this.counterpart_id))
    }

    pub fn delete(_this: &Counterpart) -> Vec<Counterpart> {
        sql!(format!("delete from counterpart where counterpart_id = {}",
        _this.counterpart_id))
    }

    pub fn update(_this: &Counterpart) -> Vec<Counterpart> {
        sql!(format!("update from counterpart set price = {}, title = {}, description = {} where counterpart_id = {}",
        _this.price, _this.title, _this.description, _this.counterpart_id))
    }
}

#[derive(Serialize, Deserialize)]
struct Crowdfunding {
    crowdfunding_id: i64,
    game_id: i64,
    objectif: i64,
    start_date: String,
    end_date: String,
    desciption: String,
}

impl Crowdfunding {
    pub fn new(crowdfunding_id:i64, game_id:i64, objectif:i64, start_date:String, end_date:String, desciption:String) -> Crowdfunding
    {
        Crowdfunding {
            crowdfunding_id,
            game_id,
            objectif,
            start_date,
            end_date,
            desciption,
        }
    }

    pub fn insert(_this:&Crowdfunding) -> Crowdfunding {
        sql!(format!("insert into crowdfunding value ({}, {}, {}, {}, {}, {})",
                _this.crowdfunding_id, _this.game_id, _this.objectif, _this.start_date, _this.end_date, _this.desciption))
    }

    pub fn select_all(_this:&Crowdfunding) -> Crowdfunding {
        sql!(format!("select * from crowdfunding where game_id = {}",
        _this.game_id))
    }

    pub fn select_all_id(_game_id:i64) -> Crowdfunding {
        sql!(format!("select * from crowdfunding where game_id = {}",
                _game_id))
    }

    pub fn select_one(_this:&Crowdfunding) -> Crowdfunding {
        sql!(format!("select * from crowdfunding where crowdfunding_id = {}",
        _this.crowdfunding_id))
    }

    pub fn delete(_this:&Crowdfunding) -> crowdfunding {
        sql!(format!("delete from counterpart where  crowdfunding_id = {}",
        _this.crowdfunding_id));
        sql!(format!("delete from crowdfunding where crowdfunding_id = {}",
        _this.crowdfunding_id))
    }

    pub fn update(_this:&Crowdfunding) -> Crowdfunding {
        sql!(format!("update crowdfunding set objectif = {}, start_date = {}, end_date = {}, desciption = {} where crowdfunding_id = {}",
                 _this.objectif, _this.start_date, _this.end_date, _this.desciption, _this.crowdfunding_id))
    }
}

#[derive(Serialize, Deserialize)]
struct Friend {
    usera_id: i64,
    userb_id: i64,
}


impl Friend {
    pub fn new(usera_id:i64, userb_id:i64) -> Friend
    {
        Friend {
            usera_id,
            userb_id
        }
    }
    pub fn insert(_this:&Friend) -> Vec<Friend> {
        sql!(format!("insert into friend value ({}, {})",
                _this.usera_id, _this.userb_id))
    }
    pub fn delete(_this:&Friend) -> Vec<Friend> {
        sql!(format!("delete from friend where usera_id = {}",
                _this.usera_id))
    }
    pub fn get_all(_this:&Friend) -> Vec<Friend>  {
        sql!(format!("select * from friend where usera_id = {} and (usera_id, userb_id) in (select userb_id, usera_id from friend))",
                _this.usera_id))
    }

    pub fn get_all_invite(_this:&Friend) -> Vec<Friend>  {
        sql!(format!("select * from friend where userb_id = {} and (usera_id, userb_id) not in (select userb_id, usera_id from friend))",
                _this.usera_id))
    }
}

#[derive(Serialize, Deserialize)]
struct Game {
    game_id: i64,
    name: String,
    desciption: String,
    price: f64,
    path: i64,
}

impl Game {
    pub fn new(game_id: i64, name: String, desciption: String, price: f64, path: i64) -> Game
    {
        Game {
            game_id,
            name,
            desciption,
            price,
            path,
        }
    }

    pub fn insert(_this:&Game) -> Game {
        sql!(format!("insert into game value ({}, {}, {}, {}, {})",
                    _this.game_id, _this.name, _this.desciption, _this.price, _this.path))
    }


    pub fn select_one(_this:&Game) -> Game {
        sql!(format!("select * from game where game_id = {}",
                    _this.game_id))
    }

    pub fn update(_this:&Game) -> Game {
        sql!(format!("update game set name = {}, desciption = {}, price = {}, path = {} where game_id = {}",
                    _this.name, _this.desciption, _this.price, _this.path, _this.game_id))
    }
}

#[derive(Serialize, Deserialize)]
struct GameTag {
    tag_id: i64,
    game_id: i64,
}

impl GameTag {
    pub fn new(tag_id: i64,game_id: i64) -> GameTag
    {
        GameTag {
            tag_id,
            game_id
        }
    }

    pub fn insert(_this:&GameTag) -> GameTag {
        sql!(format!("insert into game_tag value ({}, {})",
                    _this.tag_id, _this.game_id))
    }

    pub fn get_one(_this:&GameTag) -> GameTag {
        sql!(format!("select * from game_tag where tag_id = {} and game_id = {}",
                    _this.tag_id, _this.game_id))
    }


    pub fn get_all(_this:&GameTag) -> GameTag {
        sql!(format!("select * from game_tag where tag_id = {}",
                    _this.tag_id))
    }

    pub fn delete(_this:&GameTag) -> GameTag {
        sql!(format!("delete into game_tag value ({}, {})",
                    _this.tag_id, _this.game_id))
    }
}

#[derive(Serialize, Deserialize)]
struct Rate {
    user_id: i64,
    game_id: i64,
    note: i64,
    date: String,
}

impl Rate {
    pub fn new(user_id: i64,game_id: i64, note: i64, date:String) -> Rate
    {
        Rate {
            user_id,
            game_id,
            note,
            date,
        }
    }

    pub fn insert(_this:&Rate) -> Rate {
        sql!(format!("insert into rate value ({}, {}, {}, {})",
                    _this.tag_id, _this.game_id, _this.note, _this.date))
    }

    pub fn update(_this:&Rate) -> Rate {
        sql!(format!("update rate set note = {}, date = {} where tag_id = {} and game_id = {}",
                    _this.tag_id, _this.game_id, _this.note, _this.date))
    }

    pub fn delete(_this:&Rate) -> Rate {
        sql!(format!("delete from rate where tag_id = {} and game_id = {}",
                    _this.tag_id, _this.game_id))
    }

    pub fn get_one(_this:&Rate) -> Rate {
        sql!(format!("select AVG(note) AS note from rate where game_id = {}",
                    _this.game_id))
    }
}

#[derive(Serialize, Deserialize)]
struct Report {
    user_id: i64,
    game_id: i64,
    title: String,
    description: String,
    date: String,
}

impl Report {
    pub fn new(user_id: i64, game_id: i64, title: String, description: String, date: String) -> Report
    {
        Report {
            user_id,
            game_id,
            title,
            description,
            date,
        }
    }

    pub fn insert(_this:&Report) -> Report {
        sql!(format!("insert into report value ({}, {}, {}, {}, {})",
                    _this.user_id, _this.game_id, _this.title, _this.description, _this.date))
    }

    pub fn update(_this:&Report) -> Report {
        sql!(format!("update report set title = {}, description = {}, date = {} where user_id = {} and game_id = {}",
                    _this.title, _this.description, _this.date, _this.user_id, _this.game_id))
    }

    pub fn delete(_this:&Report) -> Report {
        sql!(format!("delete from report where user_id = {} and game_id = {}",
                    _this.user_id, _this.game_id))
    }

    pub fn get_one(_this:&Report) -> Report {
        sql!(format!("select * from report where user_id = {} and game_id = {}",
                    _this.user_id, _this.game_id))
    }

    pub fn get_all(_this:&Report) -> Report {
        sql!(format!("select * from report where game_id = {}",
                    _this.game_id))
    }
}

#[derive(Serialize, Deserialize)]
struct Step {
    step_id: i64,
    version_id: i64,
    name: String,
}

impl Step {
    pub fn new(step_id: i64, version_id: i64, name: String) -> Step
    {
        Step {
            step_id,
            version_id,
            name,
        }
    }

    pub fn insert(_this:&Step) -> Step {
        sql!(format!("insert into step value ({}, {}, {})",
                    _this.step_id, _this.version_id, _this.name))
    }

    pub fn update(_this:&Step) -> Step {
        sql!(format!("update step set name = {} where step_id = {} and version_id = {}",
                    _this.name, _this.step_id, _this.version_id))
    }

    pub fn delete(_this:&Step) -> Step {
        sql!(format!("delete from step where step_id = {} and version_id = {}",
                    _this.step_id, _this.version_id))
    }

    pub fn get_one(_this:&Step) -> Step {
        sql!(format!("select * from step where step_id = {} and version_id = {}",
                    _this.step_id, _this.version_id))
    }

    pub fn get_all(_this:&Step) -> Step {
        sql!(format!("select * from step where game_id = {}",
                    _this.game_id))
    }
}

#[derive(Serialize, Deserialize)]
struct Tag {
    tag_id: i64,
    name: String
}

#[derive(Serialize, Deserialize)]
struct Todo {
    todo_id: i64,
    version_id: i64,
    step_id: i64,
    end_date: String,
    start_date: String,
    title: String,
    desciption: String,
}

#[derive(Serialize, Deserialize)]
struct UserGameCreate {
    user_id: i64,
    game_id: i64,
    date: String
}

#[derive(Serialize, Deserialize)]
struct UserGameBuy {
    user_id: i64,
    game_id: i64,
    date: String,
}

#[derive(Serialize, Deserialize)]
struct Version {
    version_id: i64,
    game_id: i64,
    version: String,
    end_date: String,
    start_date: String,
    title: String,
    desciption: String,
}