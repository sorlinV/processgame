let url = "https://api.processgame.digitarucamera.eu";
console.log("JS loaded");

let header_game = new Vue({
    el: 'body > header',
    data: {
      token: null,
      connected: false,
      registered: false
    },
    methods: {
      overlay_connect: function() {
        this.connected = !this.connected;
      },
      overlay_register: function() {
        this.registered = !this.registered;
      },
      connect: function() {
        let myHeaders = new Headers();
        myHeaders.token = JSON.stringify(this.token);
        let myInit = { method: 'GET',
        body: new FormData(document.getElementById('login-form')),
        headers: myHeaders,
        mode: 'cors',
        cache: 'default' };
        fetch(url + "/user", myInit)
        .then(function(response) {
          console.log(response);
        });
      },
      register: function() {

      }
    }
 })
