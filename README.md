<p align="center">
	<a href="https://processgame.digitarucamera.eu"><img src="graphic_assets/logo.png" alt="Logo processgame" width="450"></a>
</p>
<h3 align="center">Make you own game<!-- Serve Confidently --></h3>
<p align="center">Processgame is a website for help developper for making game.</p>
<p align="center">
    <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LKXPKGCQPEHH6"><img src="https://img.shields.io/badge/Donate-PayPal-green.svg" alt="don paypal"></a>
</p>
<p align="center">
	<a href="">Download</a> ·
	<a href="https://processgame.digitarucamera.eu/docs">Documentation</a> ·
</p>

---

ProcessGame is a web site with api that is made for all Game creator who want to
create with simple tools and for all gamer who want too see he game process in real-time.

Site website is able on https://processgame.digitarucamera.eu

## Menu

- [Features](#features)
- [Install](#install)
- [Quick Start](#quick-start)
- [Contributing](#contributing)
- [Donors](#donors)
- [About the Project](#about-the-project)

## Features

### For GameCreator

- Organise her todo
- Organise her time
- Organise her version
- Sell her game(s)
- Make crowdfunding

### For GameBuyer

- Show game conception information
- buy game
- help GameCreator

## Install

- Download repo
- Launch `$ sudo cd processgame-server & docker-compose up`

## Quick Start

You can go [her](https://docs.processgame.DigitaruCamera.eu) for read complete documentation

## Contributing

**[Join our forum](https://caddy.community) where you can chat with other Caddy users and developers!** To get familiar with the code base, try [Caddy code search on Sourcegraph](https://sourcegraph.com/github.com/mholt/caddy/-/search)!

Please see our [contributing guidelines](https://github.com/mholt/caddy/blob/master/.github/CONTRIBUTING.md) for instructions. If you want to write a plugin, check out the [developer wiki](https://github.com/mholt/caddy/wiki).

We use GitHub issues and pull requests only for discussing bug reports and the development of specific changes. We welcome all other topics on the [forum](https://caddy.community)!

If you want to contribute to the documentation, please [submit an issue](https://github.com/mholt/caddy/issues/new) describing the change that should be made.

Thanks for making Caddy -- and the Web -- better!


## Donors

[![Donate](https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_donate_SM.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LKXPKGCQPEHH6)

## About the Project

