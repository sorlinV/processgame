<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'docs_processgame');

/** MySQL database username */
define('DB_USER', 'digitarucamera');

/** MySQL database password */
define('DB_PASSWORD', 'digitarucamera');

/** MySQL hostname */
define('DB_HOST', 'processgame-server_db_1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'h975]m;FfB:Np!z(qk.AQ[+vU]{^r-_Am7A@YfmdQl=giA9S=,6r8;fC}4o+!9fW');
define('SECURE_AUTH_KEY',  ' JzRYoH|rN9o,,f0atToLOqMPTpA?9nc!5z-16Hn*[|(N j^K5sHTJ,4zjs#>p*Y');
define('LOGGED_IN_KEY',    'l@_tR{)O}r]_scHt<I%F S_Cj8zPhi5A;ju~!5:gFK9wkc{.}420~X8F<UY7Kx a');
define('NONCE_KEY',        'qE8Bd,J_U2:9aB,wR{j2FD!X_^I/$,(5m0hve=-^<,:V)_VPKN~Hm1A,Z<Yrh3z7');
define('AUTH_SALT',        'kLQ2;$]Ae8rN<+AcK`:kQ)JGE]8I0ZGb!Vn;$t5p7z4=mldb&< }?`e67lt?xwe]');
define('SECURE_AUTH_SALT', '% Upu^5-*jxy >(O6:MTs^F<up S{Xj$@=,(TKRC5c~e$h(omF?hXYl+7{UzQ&*A');
define('LOGGED_IN_SALT',   '% vVgqMSx^L`1R8$#W0-A}2lnl/zU#a8,WE967qTlqdz]X404`nuk+.Sbtm@+sm$');
define('NONCE_SALT',       'AbSp>,;z~C4 .d@|#9&Mzd!fWo;^3`J>@Sz=x[rV7@[E0fCyABSuUu?InQjvkU-,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
