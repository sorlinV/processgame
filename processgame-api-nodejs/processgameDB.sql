drop DATABASE db_processgame;
CREATE DATABASE IF NOT EXISTS db_processgame;
USE db_processgame;

CREATE TABLE IF NOT EXISTS `user` (
`user_id` INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY COMMENT 'Primary key from table user',
`username` VARCHAR(64) NOT NULL UNIQUE COMMENT 'Username for user',
`password` VARCHAR(64) NOT NULL COMMENT 'User password encrypted with salt',
`salt` VARCHAR(64) NOT NULL COMMENT 'User salt ',
`mail` VARCHAR(64) NOT NULL COMMENT 'The user mail',
`name` VARCHAR(64) NOT NULL COMMENT 'The user firstname',
`lastname` VARCHAR(64) NOT NULL COMMENT 'The user lastname',
`description` TEXT NOT NULL COMMENT 'The descritpion of user in markdown',
`image` VARCHAR(64) NOT NULL COMMENT 'the link of user image profile'
);

CREATE TABLE IF NOT EXISTS `friend` (
`usera_id` INT UNSIGNED NOT NULL COMMENT 'the user that invite second',
`userb_id` INT UNSIGNED NOT NULL COMMENT 'the user invited',
PRIMARY KEY (`usera_id`, `userb_id`),
FOREIGN KEY (`usera_id`) REFERENCES `user` (`user_id`),
FOREIGN KEY (`userb_id`) REFERENCES `user` (`user_id`)
);

CREATE TABLE IF NOT EXISTS `game` (
`game_id` INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY COMMENT 'Primary key form table game',
`name` VARCHAR(64) NOT NULL COMMENT 'The game name',
`image` VARCHAR(64) NOT NULL COMMENT 'the link of game image',
`description` TEXT NOT NULL COMMENT 'The description of game in markdown',
`price` DECIMAL(64, 2) NOT NULL COMMENT 'the price of game',
`reduction` INT NOT NULL COMMENT 'the percentage of réduction of game',
`link` VARCHAR(64) NOT NULL COMMENT 'the link of game zipped'
);

CREATE TABLE IF NOT EXISTS `tag` (
`tag_id` INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY COMMENT 'primary key from table tag',
`name` VARCHAR(64) NOT NULL COMMENT 'The tag text'
);

CREATE TABLE IF NOT EXISTS `game_tag` (
`tag_id` INT UNSIGNED NOT NULL COMMENT 'primary key from table tag',
`game_id` INT UNSIGNED NOT NULL COMMENT 'Primary key form table game',
PRIMARY KEY (`tag_id`, `game_id`),
FOREIGN KEY (`tag_id`) REFERENCES `tag` (tag_id),
FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`)
);

CREATE TABLE IF NOT EXISTS `crowdfunding` (
`crowdfunding_id` INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY COMMENT 'primary key from table crowdfunding',
`game_id` INT UNSIGNED NOT NULL COMMENT 'Primary key form table game',
`objectif` DECIMAL(64, 2) NOT NULL COMMENT 'minimum total amount of money required',
`start_date` DATETIME COMMENT 'The start date of crowdfunding',
`end_date` DATETIME COMMENT 'The end date of crowdfunding',
`description` TEXT NOT NULL COMMENT 'The descritpion of crowdfunding in markdown',
FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`)
);

CREATE TABLE IF NOT EXISTS `counterpart` (
`counterpart_id` INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY COMMENT 'primary key from counterpart',
`crowdfunding_id` INT UNSIGNED NOT NULL COMMENT 'primary key from crowdfunding',
`price` DECIMAL(64, 2) NOT NULL COMMENT 'the minimal money for reach',
`title` VARCHAR(64) NOT NULL COMMENT 'the title of counterpart',
`description` TEXT NOT NULL COMMENT 'The descritpion of counterpart in markdown',
FOREIGN KEY (`crowdfunding_id`) REFERENCES `crowdfunding` (`crowdfunding_id`)
);

CREATE TABLE IF NOT EXISTS `version` (
`version_id` INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY COMMENT 'primary key from table version',
`game_id` INT UNSIGNED NOT NULL COMMENT 'primary key from table game',
`version` VARCHAR(64) NOT NULL COMMENT 'the version for game example: 1.12.45',
`start_date` DATETIME COMMENT 'the start date of version',
`end_date` DATETIME COMMENT 'the expected end date for version',
`title` VARCHAR(64) NOT NULL COMMENT 'the title of version example bug patch version',
`description` TEXT NOT NULL COMMENT 'The descritpion of counterpart in markdown',
FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`)
);

CREATE TABLE IF NOT EXISTS `step` (
`step_id` INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY COMMENT 'primary key from table step',
`version_id` INT UNSIGNED NOT NULL COMMENT 'primary key from table version',
`name` VARCHAR(64) NOT NULL COMMENT 'comment',
FOREIGN KEY (`version_id`) REFERENCES `version` (`version_id`)
);

CREATE TABLE IF NOT EXISTS `todo` (
`todo_id` INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY COMMENT 'primary key from table todo',
`version_id` INT UNSIGNED NOT NULL COMMENT 'primary key from table version',
`step_id` INT UNSIGNED NOT NULL COMMENT 'primary key from table step',
`start_date` DATETIME COMMENT 'the start date of todo',
`end_date` DATETIME COMMENT 'the expected end date for todo',
`title` VARCHAR(64) NOT NULL COMMENT 'the title of todo',
`description` TEXT NOT NULL COMMENT 'the description of todo in markdown',
FOREIGN KEY (`version_id`) REFERENCES `version` (`version_id`),
FOREIGN KEY (`step_id`) REFERENCES `step` (`step_id`)
);

CREATE TABLE IF NOT EXISTS `user_game_create` (
`user_id` INT UNSIGNED NOT NULL COMMENT 'primary key from table user',
`game_id` INT UNSIGNED NOT NULL COMMENT 'primary key from table game',
`date` DATETIME COMMENT 'join date of game',
PRIMARY KEY (`user_id`, `game_id`),
FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`)
);

CREATE TABLE IF NOT EXISTS `user_game_buy` (
`user_id` INT UNSIGNED NOT NULL COMMENT 'primary key from table user',
`game_id` INT UNSIGNED NOT NULL COMMENT 'primary key from table game',
`date` DATETIME COMMENT 'join date of game',
PRIMARY KEY (`user_id`, `game_id`),
FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`)
);

CREATE TABLE IF NOT EXISTS `cart` (
`user_id` INT UNSIGNED NOT NULL COMMENT 'primary key from table user',
`game_id` INT UNSIGNED NOT NULL COMMENT 'primary key from table game',
PRIMARY KEY (`user_id`, `game_id`),
FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`)
);

CREATE TABLE IF NOT EXISTS `rate` (
`user_id` INT UNSIGNED NOT NULL COMMENT 'primary key from table user',
`game_id` INT UNSIGNED NOT NULL COMMENT 'primary key from table game',
`note` INT UNSIGNED NOT NULL COMMENT 'note for game by player',
`date` DATETIME COMMENT 'date of vote',
PRIMARY KEY (`user_id`, `game_id`),
FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`)
);

CREATE TABLE IF NOT EXISTS `report` (
`user_id` INT UNSIGNED NOT NULL COMMENT 'primary key from table user',
`game_id` INT UNSIGNED NOT NULL COMMENT 'primary key from table game',
`title` VARCHAR(64) NOT NULL COMMENT 'the title of report',
`description` TEXT NOT NULL COMMENT 'the description of report in markdown',
`date` DATETIME COMMENT 'date of report creation',
PRIMARY KEY (`user_id`, `game_id`),
FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
FOREIGN KEY (`game_id`) REFERENCES `game` (`game_id`)
);
