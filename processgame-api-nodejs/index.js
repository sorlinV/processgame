const express = require("express");
const bodyParser = require("body-parser");
const mysql = require('mysql');
const config = require('config-yml');
const fs = require('fs');
const crypto = require('crypto');
const User = require('./Classes/User.js');
const Game = require('./Classes/Game.js');
const Message = require('./Classes/Message.js');
const Token = require('./Classes/Token.js');
const app = express();
const MESSAGE_OK = 0;
const MESSAGE_ERROR = 1;
const MESSAGE_WARNING = 2;
const MESSAGE_HIDDEN = 3;

function sha256(_str)
{
    const secret = 'digitarucamera';
    return crypto.createHmac('sha256', secret)
                   .update(_str)
                   .digest('hex');
}

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(function (req, res, next) {
    //Enabling CORS 
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
});

console.log("Config SQL:")
console.log(config.sql);
let sql
function connectServer()
{
    sql = mysql.createConnection({
        host: config.sql.host,
        user: config.sql.user,
        password: config.sql.password
    });
    sql.connect(function(err) {
        if (err)
        {
            console.error("En attente de connection au serveur sql");
            setTimeout(connectServer, 5000);
        }
        else
        {
            let installSQL = " USE db_processgame";
            
            sql.query(installSQL, function (err, res) {
                if (err) {
                    console.error("probleme avec le fichier sql");
                }
                let server = app.listen(process.env.PORT || config.port, function () {
                    let port = server.address().port;
                    console.log("App now running on port", port);
                });
            });
        }
    });
}
connectServer();

/* USER */
/* connect user */
app.get("/user", function(req , Output){
    if (req.body.username && req.body.password)
    {
        let query = `select * from user where username = '${req.body.username}'`;
        sql.query(query, function (err, res) {
            if (err) Output.send(new Message(MESSAGE_ERROR, "error in /user"));
            else {
                if (res.password === sha256(req.body.password + res.salt))
                    Output.send(new Token(res.user_id, res.password, res.username));
                else
                    Output.send(new Message(MESSAGE_ERROR, "bad password or password"));
            }
        });    
    }
});

/* register user */
app.post("/user", function(req , Output){
    let user = new User(req.body.username, req.body.password, " ", req.body.mail, req.body.name, req.body.lastname, req.body.description, req.body.image);
    sql.query(user.insert(), function (err, res) {
        if (err)
        {
            Output.send(new Message(MESSAGE_ERROR, "register error invalid information " + err));
        }
        else
        {
            Output.send(new Token(res.insertId, user.password, user.username));
        }
    });
});

/* edit user */
app.put("/user/:user_id", function(req , Output){
    let Token = JSON.parse(req.headers.token);
    let query = `select * from user where user_id = ${Token.user_id};`;
    sql.query(query, function (err, res) {
        if (err) Output.send(new Message(MESSAGE_ERROR, "error in /user/:user_id"));
        else
        {
            let userToken = new User(res[0].username, res[0].password, res[0].salt, res[0].mail, res[0].name, res[0].lastname, res[0].description, res[0].image);
            if (userToken.password === Token.token)
            {
                /* token valide */
                let user = new User(req.body.username, req.body.password, " ", req.body.mail, req.body.name, req.body.lastname, req.body.description, req.body.image);
                sql.query(user.update(Token.user_id), function(err, res)
                {
                    if (err) Output.send(new Message(MESSAGE_ERROR, "error in /user/:user_id"));
                    else Output.send(new Message(MESSAGE_OK, "User modified"));
                });
            }
            else
            {
                Output.send(new Message(MESSAGE_ERROR, "invalid Token"));
            }
        }
    });
});

/* delete user */
app.delete("/user/:user_id", function(req , Output){
    let Token = JSON.parse(req.headers.token);
    let query = `select * from user where user_id = ${req.params.user_id};`;
    sql.query(query, function (err, res) {
        if (err) Output.send(new Message(MESSAGE_ERROR, "error in /user/:user_id"));
        else {
            let userParam = new User(res[0].username, res[0].password, res[0].salt, res[0].mail, res[0].name, res[0].lastname, res[0].description, res[0].image);
            if (userParam.password === Token.token)
            {
                /* token valide */
                let user = new User(req.body.username, req.body.password, " ", req.body.mail, req.body.name, req.body.lastname, req.body.description, req.body.image);
                sql.query(user.delete(Token.user_id), function(err, res)
                {
                    if (err) Output.send(new Message(MESSAGE_ERROR, "error in /user/:user_id"));
                    else Output.send(new Message(MESSAGE_OK, "User delete"));
                });
            }
            else
            {
                Output.send(new Message(MESSAGE_ERROR, "invalid Token"));
            }
        }
    });
});

/* add friend */
app.post("/user/addFriend/:user_id", function(req , Output){
    let Token = JSON.parse(req.headers.token);
    let query = `select * from user where user_id = '${Token.user_id}'`;
    sql.query(query, function (err, res) {
        if (err) Output.send(new Message(MESSAGE_ERROR, "error in /user/addFriend/:user_id"));
        else
        {
            let userToken = new User(res[0].username, res[0].password, res[0].salt, res[0].mail, res[0].name, res[0].lastname, res[0].description, res[0].image);
            if (userToken.password === Token.token)
            {
                /* token valide */
                let query = `insert into friend(usera_id, userb_id) values('${Token.user_id}', '${req.params.user_id}')`;
                sql.query(query, function(err, res)
                {
                    Output.send(new Message(MESSAGE_OK, "Friend added"));
                });
            }
            else
            {
                Output.send(new Message(MESSAGE_ERROR, "invalid Token"));
            }
        }
    });
});

/* GAME */
/* add game */
app.post("/game", function(req , Output){
    let Token = JSON.parse(req.headers.token);
    let query = `select * from user where user_id = '${Token.user_id}'`;
    sql.query(query, function (err, res) {
        if (err) Output.send(new Message(MESSAGE_ERROR, "error in /game"));
        else
        {
            let userToken = new User(res[0].username, res[0].password, res[0].salt, res[0].mail, res[0].name, res[0].lastname, res[0].description, res[0].image);
            if (userToken.password === Token.token)
            {
                /* token valide */
                let game = new Game(req.body.name, req.body.description, req.body.image, req.body.price, req.body.reduction, req.body.link);
                sql.query(game.insert(), function(err, res)
                {
                    if (err) Output.send(new Message(MESSAGE_ERROR, "failed to create game"));
                    else {
                        sql.query(game.linkCreator(res.insertId, Token.user_id), function(err, res)
                        {
                            if (err) Output.send(new Message(MESSAGE_ERROR, "failed to create user_game_creation"));
                            else Output.send(new Message(MESSAGE_OK, "Game created"));
                        });
                    }
                });
            }
            else
            {
                Output.send(new Message(MESSAGE_ERROR, "invalid Token"));
            }
        }
    });
});

/* buy game */
app.post("/game/buy/:game_id", function(req , Output){
    let Token = JSON.parse(req.headers.token);
    let query = `select * from user where user_id = '${Token.user_id}'`;
    sql.query(query, function (err, res) {
        if (err) Output.send(new Message(MESSAGE_ERROR, "invalid Token"));
        else {
            let userToken = new User(res[0].username, res[0].password, res[0].salt, res[0].mail, res[0].name, res[0].lastname, res[0].description, res[0].image);
            if (userToken.password === Token.token)
            {
                /* token valide */
                let game = new Game(req.body.name, req.body.description, req.body.image, req.body.price, req.body.reduction, req.body.link);
                sql.query(game.linkBuy(req.params.game_id, Token.user_id), function(err, res)
                {
                    if (err) Output.send(new Message(MESSAGE_ERROR, "failed to create user_game_buy " + err));
                    else Output.send(new Message(MESSAGE_OK, "Game buyed"));
                });
            }
            else
            {
                Output.send(new Message(MESSAGE_ERROR, "invalid Token"));
            }
        }
    });
});

/* get game for shop */
app.get("/game/shop", function(req , Output){
    let query = `select * from game`;
    if (req.headers.token)
    {
        let Token = JSON.parse(req.headers.token);
        query = `select * from game where game_id not in (select game_id from user_game_create where user_id = ${Token.user_id})
                                    and game_id not in (select game_id from user_game_buy where user_id = ${Token.user_id})`;
    }
    sql.query(query, function (err, res) {
        if (err) Output.send(new Message(MESSAGE_ERROR, "error in /game/shop"));
        else Output.send(res);
    });
});

/* get game for library */
app.get("/game/library", function(req , Output){
    if (req.headers.token)
    {
        let Token = JSON.parse(req.headers.token);
        let query = `select * from user where user_id = '${Token.user_id}'`;
        sql.query(query, function (err, res) {
            let userToken = new User(res[0].username, res[0].password, res[0].salt, res[0].mail, res[0].name, res[0].lastname, res[0].description, res[0].image);
            if (userToken.password === Token.token)
            {
                let query = `select * from game where game_id in (select game_id from user_game_buy where user_id = ${Token.user_id})`;
                sql.query(query, function (err, res) {
                    if (err) Output.send(new Message(MESSAGE_ERROR, "error in /game/library"));
                    else Output.send(res);
                });
            }
            else
            {
                Output.send(new Message(MESSAGE_ERROR, "invalid Token"));
            }
        });
    }
    else
    {
        Output.send(new Message(MESSAGE_ERROR, "no Token"));
    }
});

/* game for page creation */
app.get("/game/creation", function(req , Output){
    let Token = JSON.parse(req.headers.token);
    let query = `select * from user where user_id = '${Token.user_id}'`;
    sql.query(query, function (err, res) {
        let userToken = new User(res[0].username, res[0].password, res[0].salt, res[0].mail, res[0].name, res[0].lastname, res[0].description, res[0].image);
        if (userToken.password === Token.token)
        {
            let query = `select * from game where game_id in (select game_id from user_game_create where user_id = ${Token.user_id})`;
            sql.query(query, function (err, res) {
                if (err) Output.send(new Message(MESSAGE_ERROR, "error in /game/creation"));
                else Output.send(res);
            });
        }
        else
        {
            Output.send(new Message(MESSAGE_ERROR, "invalid Token"));
        }
    });
});

/* get game */
app.get("/game/:game_id", function(req , Output){
    let Token = JSON.parse(req.headers.token);
    let query = `select * from user where user_id = '${Token.user_id}'`;
    sql.query(query, function (err, res) {
        let userToken = new User(res[0].username, res[0].password, res[0].salt, res[0].mail, res[0].name, res[0].lastname, res[0].description, res[0].image);
        if (userToken.password === Token.token)
        {
            let query = `select *,
                        (select count(*) from user_game_create where game_id = ${req.params.game_id} and user_id = ${Token.user_id}) AS "is_creation",
                        (select count(*) from user_game_buy where game_id = ${req.params.game_id} and user_id = ${Token.user_id}) AS "is_buyed",
                         from game where game_id = ${req.params.game_id}`;
            sql.query(query, function (err, res) {
                if (err) Output.send(new Message(MESSAGE_ERROR, "error in /game/:game_id"));
                else Output.send(res);
            });
        }
        else
        {
            Output.send(new Message(MESSAGE_ERROR, "invalid Token"));
        }
    });
});

/* edit game */
app.put("/game/:game_id", function(req , Output){
    let Token = JSON.parse(req.headers.token);
    let query = `select * from user where user_id = '${Token.user_id}'`;
    sql.query(query, function (err, res) {
        let userToken = new User(res[0].username, res[0].password, res[0].salt, res[0].mail, res[0].name, res[0].lastname, res[0].description, res[0].image);
        if (userToken.password === Token.token)
        {
            let game = new Game(req.body.name, req.body.description, req.body.image, req.body.price, req.body.reduction, req.body.link);
            sql.query(game.update(req.params.game_id, Token.user_id), function(err, res) {
                if (err)
                {
                    Output.send(new Message(MESSAGE_ERROR, "Modification error"));
                }
                else
                {
                    Output.send(new Message(MESSAGE_OK, "Game modified"));
                }
            });
        }
        else
        {
            Output.send(new Message(MESSAGE_ERROR, "invalid Token"));
        }
    });
});

/* delete game */
app.delete("/game/:game_id", function(req , Output){
    let Token = JSON.parse(req.headers.token);
    let query = `select * from user where user_id = '${Token.user_id}'`;
    sql.query(query, function (err, res) {
        let userToken = new User(res[0].username, res[0].password, res[0].salt, res[0].mail, res[0].name, res[0].lastname, res[0].description, res[0].image);
        if (userToken.password === Token.token)
        {
            let game = new Game(req.body.name, req.body.description, req.body.image, req.body.price, req.body.reduction, req.body.link);
            sql.query(game.delete(req.params.game_id, Token.user_id), function(err, res) {
                if (err) Output.send(new Message(MESSAGE_ERROR, "delete error"));
                else Output.send(new Message(MESSAGE_OK, "Game deleted"));
            });
        }
        else
        {
            Output.send(new Message(MESSAGE_ERROR, "invalid Token"));
        }
    });
});

app.get("/hello", function(req , Output){
    Output.send(new Message(MESSAGE_HIDDEN, "Hello processgame user !"));
});