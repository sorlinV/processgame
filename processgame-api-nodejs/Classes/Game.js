
class Game {
    constructor(_name, _description, _image, _price, _reduction, _link) {
        if (_name !== undefined)
        {
            this.name = _name;
            this.description = _description;
            this.image = _image;
            this.price = _price;
            this.reduction = _reduction;
            this.link = _link;
        }
    }

    insert ()
    {
        return `insert into game(name, description, image, price, reduction, link) values ('${this.name}', '${this.description}', '${this.image}', ${this.price}, ${this.reduction}, '${this.link}');`;
    }

    update (_game_id, _user_id)
    {
        return `update game set name = '${this.name}', description = '${this.description}', image = '${this.image}', price = ${this.price}, reduction = ${this.reduction}, link = '${this.link}'
                        where game_id = ${_game_id} and game_id in (select game_id from user_game_create where game_id = ${_game_id} and user_id = ${_user_id })`;
    }

    delete (_game_id, _user_id)
    {
        return `delete from game
                        where game_id = ${_game_id} and game_id in (select game_id from user_game_create where game_id = ${_game_id} and user_id = ${_user_id });
                        delete from user_game_create  where game_id = ${_game_id} and user_id = ${_user_id }`;
    }

    linkCreator (_game_id, _user_id)
    {
        return `insert into user_game_create(user_id, game_id, date) values ('${_user_id}', '${_game_id}', SYSDATE(4))`;
    }

    linkBuy (_game_id, _user_id)
    {
        return `insert into user_game_buy(user_id, game_id, date) values ('${_user_id}', '${_game_id}', SYSDATE(4))`;
    }
}

module.exports = Game;