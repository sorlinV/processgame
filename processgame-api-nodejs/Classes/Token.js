class Token {
    constructor(_user_id, _token, _username) {
        this.user_id = _user_id;
        this.token = _token;
        this.username = _username;
    }
}

module.exports = Token;