class User {
    constructor(_username, _password, _salt, _mail, _name, _lastname, _description, _image) {
        if (_username !== undefined)
        {
            this.username = _username;
            this.password = _password;
            this.salt = _salt;
            this.mail = _mail;
            this.name = _name;
            this.lastname = _lastname;
            this.description = _description;
            this.image = _image;    
        }
    }

    insert ()
    {
        this.salt = sha256('' + Math.random());
        this.password = sha256(this.password + this.salt);
        return "insert into user(username, password, salt, mail, name, lastname, description, image)" +
         `values ('${this.username}', '${this.password}', '${this.salt}', '${this.mail}', '${this.name}', '${this.lastname}', '${this.description}', '${this.image}')`;
    }

    update (_user_id)
    {
        this.salt = sha256('' + Math.random());
        this.password = sha256(this.password + this.salt);
        return `update user set username = '${this.username}', password = '${this.password}',
        salt = '${this.salt}', name = '${this.name}', lastname = '${this.lastname}', description = '${this.description}', image = '${this.image}'
        where user_id = ${_user_id}`;
    }

    delete (_user_id)
    {
        return `delete from user where user_id = '${_user_id}'`;
    }
}

module.exports = User;