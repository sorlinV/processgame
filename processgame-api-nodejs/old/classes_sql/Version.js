'use strict';

var utils = require('../utils/writer.js');
var Version = require('../service/VersionService');

module.exports.addVersion = function addVersion (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var body = req.swagger.params['body'].value;
  Version.addVersion(game_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteVersion = function deleteVersion (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var version_id = req.swagger.params['version_id'].value;
  var body = req.swagger.params['body'].value;
  Version.deleteVersion(game_id,version_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.editVersion = function editVersion (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var version_id = req.swagger.params['version_id'].value;
  var body = req.swagger.params['body'].value;
  Version.editVersion(game_id,version_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
