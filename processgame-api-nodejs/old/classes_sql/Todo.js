'use strict';

var utils = require('../utils/writer.js');
var Todo = require('../service/TodoService');

module.exports.addTodo = function addTodo (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var version_id = req.swagger.params['version_id'].value;
  var step_id = req.swagger.params['step_id'].value;
  var body = req.swagger.params['body'].value;
  Todo.addTodo(game_id,version_id,step_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteTodo = function deleteTodo (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var version_id = req.swagger.params['version_id'].value;
  var step_id = req.swagger.params['step_id'].value;
  var todo_id = req.swagger.params['todo_id'].value;
  var body = req.swagger.params['body'].value;
  Todo.deleteTodo(game_id,version_id,step_id,todo_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.editTodo = function editTodo (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var version_id = req.swagger.params['version_id'].value;
  var step_id = req.swagger.params['step_id'].value;
  var todo_id = req.swagger.params['todo_id'].value;
  var body = req.swagger.params['body'].value;
  Todo.editTodo(game_id,version_id,step_id,todo_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAlltodo = function getAlltodo (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var version_id = req.swagger.params['version_id'].value;
  var step_id = req.swagger.params['step_id'].value;
  var body = req.swagger.params['body'].value;
  Todo.getAlltodo(game_id,version_id,step_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getTodo = function getTodo (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var version_id = req.swagger.params['version_id'].value;
  var step_id = req.swagger.params['step_id'].value;
  var todo_id = req.swagger.params['todo_id'].value;
  var body = req.swagger.params['body'].value;
  Todo.getTodo(game_id,version_id,step_id,todo_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
