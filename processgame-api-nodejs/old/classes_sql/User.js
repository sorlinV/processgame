'use strict';

var utils = require('../utils/writer.js');
var User = require('../service/UserService');

module.exports.addFriend = function addFriend (req, res, next) {
  var user_id = req.swagger.params['user_id'].value;
  var body = req.swagger.params['body'].value;
  User.addFriend(user_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.connectUser = function connectUser (req, res, next) {
  var body = req.swagger.params['body'].value;
  User.connectUser(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.createUser = function createUser (req, res, next) {
  var body = req.swagger.params['body'].value;
  User.createUser(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteUser = function deleteUser (req, res, next) {
  var body = req.swagger.params['body'].value;
  User.deleteUser(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.editUser = function editUser (req, res, next) {
  var body = req.swagger.params['body'].value;
  User.editUser(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getUserById = function getUserById (req, res, next) {
  var user_id = req.swagger.params['user_id'].value;
  var body = req.swagger.params['body'].value;
  User.getUserById(user_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
