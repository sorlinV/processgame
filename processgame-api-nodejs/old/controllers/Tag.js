'use strict';

var utils = require('../utils/writer.js');
var Tag = require('../service/TagService');

module.exports.addTag = function addTag (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var tag_name = req.swagger.params['tag_name'].value;
  var body = req.swagger.params['body'].value;
  Tag.addTag(game_id,tag_name,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteTag = function deleteTag (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var tag_name = req.swagger.params['tag_name'].value;
  var body = req.swagger.params['body'].value;
  Tag.deleteTag(game_id,tag_name,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAllTag = function getAllTag (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var body = req.swagger.params['body'].value;
  Tag.getAllTag(game_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
