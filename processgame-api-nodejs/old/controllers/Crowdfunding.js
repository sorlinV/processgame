'use strict';

var utils = require('../utils/writer.js');
var Crowdfunding = require('../service/CrowdfundingService');

module.exports.addCrowdfunding = function addCrowdfunding (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var body = req.swagger.params['body'].value;
  Crowdfunding.addCrowdfunding(game_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteCrowdfunding = function deleteCrowdfunding (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var crowdfunding_id = req.swagger.params['crowdfunding_id'].value;
  var body = req.swagger.params['body'].value;
  Crowdfunding.deleteCrowdfunding(game_id,crowdfunding_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.editCrowdfunding = function editCrowdfunding (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var crowdfunding_id = req.swagger.params['crowdfunding_id'].value;
  var body = req.swagger.params['body'].value;
  Crowdfunding.editCrowdfunding(game_id,crowdfunding_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAllCrowdfunding = function getAllCrowdfunding (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var body = req.swagger.params['body'].value;
  Crowdfunding.getAllCrowdfunding(game_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAllVersion = function getAllVersion (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var body = req.swagger.params['body'].value;
  Crowdfunding.getAllVersion(game_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getCrowdfunding = function getCrowdfunding (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var crowdfunding_id = req.swagger.params['crowdfunding_id'].value;
  var body = req.swagger.params['body'].value;
  Crowdfunding.getCrowdfunding(game_id,crowdfunding_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getVersion = function getVersion (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var version_id = req.swagger.params['version_id'].value;
  var body = req.swagger.params['body'].value;
  Crowdfunding.getVersion(game_id,version_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
