'use strict';

var utils = require('../utils/writer.js');
var Game = require('../service/GameService');

module.exports.createGame = function createGame (req, res, next) {
  var body = req.swagger.params['body'].value;
  Game.createGame(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteGame = function deleteGame (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var body = req.swagger.params['body'].value;
  Game.deleteGame(game_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.editGame = function editGame (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var body = req.swagger.params['body'].value;
  Game.editGame(game_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getCreation = function getCreation (req, res, next) {
  var body = req.swagger.params['body'].value;
  Game.getCreation(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getGameById = function getGameById (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var body = req.swagger.params['body'].value;
  Game.getGameById(game_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getLibrary = function getLibrary (req, res, next) {
  var body = req.swagger.params['body'].value;
  Game.getLibrary(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getShop = function getShop (req, res, next) {
  var body = req.swagger.params['body'].value;
  Game.getShop(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
