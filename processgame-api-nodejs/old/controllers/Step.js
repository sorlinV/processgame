'use strict';

var utils = require('../utils/writer.js');
var Step = require('../service/StepService');

module.exports.addStep = function addStep (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var version_id = req.swagger.params['version_id'].value;
  var body = req.swagger.params['body'].value;
  Step.addStep(game_id,version_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteStep = function deleteStep (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var version_id = req.swagger.params['version_id'].value;
  var step_id = req.swagger.params['step_id'].value;
  var body = req.swagger.params['body'].value;
  Step.deleteStep(game_id,version_id,step_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAllStep = function getAllStep (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var version_id = req.swagger.params['version_id'].value;
  var body = req.swagger.params['body'].value;
  Step.getAllStep(game_id,version_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getStep = function getStep (req, res, next) {
  var game_id = req.swagger.params['game_id'].value;
  var version_id = req.swagger.params['version_id'].value;
  var step_id = req.swagger.params['step_id'].value;
  var body = req.swagger.params['body'].value;
  Step.getStep(game_id,version_id,step_id,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
