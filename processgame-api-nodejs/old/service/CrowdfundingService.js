'use strict';


/**
 * add crowdfunding to game
 * 
 *
 * game_id Integer The game id
 * body Crowdfunding Version object
 * no response value expected for this operation
 **/
exports.addCrowdfunding = function(game_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * delete crowdfunding from game
 * 
 *
 * game_id Integer The game id
 * crowdfunding_id Integer The crowdfunding id
 * body Token Delete game object
 * no response value expected for this operation
 **/
exports.deleteCrowdfunding = function(game_id,crowdfunding_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * edit crowdfunding from game
 * 
 *
 * game_id Integer The game id
 * crowdfunding_id Integer The crowdfunding id
 * body Crowdfunding Delete game object
 * no response value expected for this operation
 **/
exports.editCrowdfunding = function(game_id,crowdfunding_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * get all crowdfunding from game
 * 
 *
 * game_id Integer The game id
 * body Token Delete game object
 * no response value expected for this operation
 **/
exports.getAllCrowdfunding = function(game_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * get all version from game
 * 
 *
 * game_id Integer The game id
 * body Token Token object
 * no response value expected for this operation
 **/
exports.getAllVersion = function(game_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * get crowdfunding from game
 * 
 *
 * game_id Integer The game id
 * crowdfunding_id Integer The crowdfunding id
 * body Token Delete game object
 * no response value expected for this operation
 **/
exports.getCrowdfunding = function(game_id,crowdfunding_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * get version from game
 * 
 *
 * game_id Integer The game id
 * version_id Integer The version id
 * body Token Token object
 * no response value expected for this operation
 **/
exports.getVersion = function(game_id,version_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

