'use strict';


/**
 * add version to game
 * 
 *
 * game_id Integer The game id
 * body Version Version object
 * no response value expected for this operation
 **/
exports.addVersion = function(game_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * delete version from game
 * 
 *
 * game_id Integer The game id
 * version_id Integer The version id
 * body Token Version object
 * no response value expected for this operation
 **/
exports.deleteVersion = function(game_id,version_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * edit version from game
 * 
 *
 * game_id Integer The game id
 * version_id Integer The version id
 * body Version Version object
 * no response value expected for this operation
 **/
exports.editVersion = function(game_id,version_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

