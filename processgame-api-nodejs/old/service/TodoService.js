'use strict';


/**
 * add todo to game version
 * 
 *
 * game_id Integer The game id
 * version_id Integer The version id
 * step_id Integer The step id
 * body Todo Token object
 * no response value expected for this operation
 **/
exports.addTodo = function(game_id,version_id,step_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * delete todo from game version step
 * 
 *
 * game_id Integer The game id
 * version_id Integer The version id
 * step_id Integer The step id
 * todo_id Integer The todo id
 * body Token Version object
 * no response value expected for this operation
 **/
exports.deleteTodo = function(game_id,version_id,step_id,todo_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * edit todo from game version step
 * 
 *
 * game_id Integer The game id
 * version_id Integer The version id
 * step_id Integer The step id
 * todo_id Integer The todo id
 * body Version Version object
 * no response value expected for this operation
 **/
exports.editTodo = function(game_id,version_id,step_id,todo_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * get all todo from game version
 * 
 *
 * game_id Integer The game id
 * version_id Integer The version id
 * step_id Integer The step id
 * body Token Token object
 * no response value expected for this operation
 **/
exports.getAlltodo = function(game_id,version_id,step_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * get todo from game
 * 
 *
 * game_id Integer The game id
 * version_id Integer The version id
 * step_id Integer The step id
 * todo_id Integer The todo id
 * body Token Token object
 * no response value expected for this operation
 **/
exports.getTodo = function(game_id,version_id,step_id,todo_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

