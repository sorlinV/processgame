'use strict';


/**
 * Get user by id
 * 
 *
 * user_id Integer The user id
 * body Token Created user object
 * returns Message
 **/
exports.addFriend = function(user_id,body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "code" : 0,
  "type" : "type",
  "message" : "message",
  "token" : {
    "user_id" : 0,
    "token" : "token",
    "username" : "username"
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * connect user
 * This can only be done by the logged in user.
 *
 * body User Created user object
 * no response value expected for this operation
 **/
exports.connectUser = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Create user
 * 
 *
 * body User Created user object
 * no response value expected for this operation
 **/
exports.createUser = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Delete user
 * 
 *
 * body User Created user object
 * no response value expected for this operation
 **/
exports.deleteUser = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Edit user
 * 
 *
 * body User Created user object
 * no response value expected for this operation
 **/
exports.editUser = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Get user by id
 * 
 *
 * user_id Integer The user id
 * body User Created user object
 * returns User
 **/
exports.getUserById = function(user_id,body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "image" : "image",
  "mail" : "mail",
  "user_id" : 6,
  "name" : "name",
  "description" : "description",
  "token" : {
    "user_id" : 0,
    "token" : "token",
    "username" : "username"
  },
  "username" : "username",
  "lastname" : "lastname"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

