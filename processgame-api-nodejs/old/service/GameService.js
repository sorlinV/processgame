'use strict';


/**
 * Create game
 * 
 *
 * body Game Created game object
 * no response value expected for this operation
 **/
exports.createGame = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * delete game
 * 
 *
 * game_id Integer The game id
 * body Game Version object
 * no response value expected for this operation
 **/
exports.deleteGame = function(game_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Edit game
 * 
 *
 * game_id Integer The game id
 * body Message Edit game object
 * no response value expected for this operation
 **/
exports.editGame = function(game_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Get all games
 * 
 *
 * body Search Edit game object
 * no response value expected for this operation
 **/
exports.getCreation = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Get game
 * 
 *
 * game_id Integer The game id
 * body Game Edit game object
 * no response value expected for this operation
 **/
exports.getGameById = function(game_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Get all games
 * 
 *
 * body Search Edit game object
 * no response value expected for this operation
 **/
exports.getLibrary = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Get all games
 * 
 *
 * body Search Edit game object
 * no response value expected for this operation
 **/
exports.getShop = function(body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

