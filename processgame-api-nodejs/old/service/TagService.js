'use strict';


/**
 * add tag to game
 * 
 *
 * game_id Integer The game id
 * tag_name Integer The tag name
 * body Game Version object
 * no response value expected for this operation
 **/
exports.addTag = function(game_id,tag_name,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * delete tag from game
 * 
 *
 * game_id Integer The game id
 * tag_name Integer The tag name
 * body Game Version object
 * no response value expected for this operation
 **/
exports.deleteTag = function(game_id,tag_name,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * get all tag from game
 * 
 *
 * game_id Integer The game id
 * body Game Version object
 * no response value expected for this operation
 **/
exports.getAllTag = function(game_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

