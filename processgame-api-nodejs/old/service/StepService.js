'use strict';


/**
 * add step to game
 * 
 *
 * game_id Integer The game id
 * version_id Integer The version id
 * body Step Step object
 * no response value expected for this operation
 **/
exports.addStep = function(game_id,version_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * delete step version from game
 * 
 *
 * game_id Integer The game id
 * version_id Integer The version id
 * step_id Integer The step id
 * body Token Version object
 * no response value expected for this operation
 **/
exports.deleteStep = function(game_id,version_id,step_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * get all step from game
 * 
 *
 * game_id Integer The game id
 * version_id Integer The version id
 * body Token Token object
 * no response value expected for this operation
 **/
exports.getAllStep = function(game_id,version_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * get step from game version
 * 
 *
 * game_id Integer The game id
 * version_id Integer The version id
 * step_id Integer The step id
 * body Token Token object
 * no response value expected for this operation
 **/
exports.getStep = function(game_id,version_id,step_id,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

